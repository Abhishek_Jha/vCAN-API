﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using LibUsbDotNet;
using LibUsbDotNet.Main;

namespace CAN_BUS_API
{
    public class vCAN_API
    {
        UsbEndpointReader reader;
        UsbEndpointWriter writer;
        public UsbDevice MyUsbDevice;
        public Guid devInterface;
        public UsbDeviceFinder MyUsbFinder;
        private byte[] pbWriteBuffer;
        private int WriterTimeOut;
        private int ReaderTimeOut;
        private byte kbps_20;
        private byte kbps_50;
        private byte kbps_100;
        private byte kbps_250;
        private byte kbps_300;
        private byte kbps_400;
        private byte kbps_500;
        private byte kbps_600;
        private byte kbps_1000;
        private byte OperatingMode_loopback;
        private byte OperatingMode_normal;
        private byte OperatingMode_listen_only;
        private byte RxMsgOnly;
        private byte TxMsgOnly;
        private byte MultipleMsg;
        public string DllVer;

        public vCAN_API()
        {
            InitializeDeviceParameter();
        }

        private void InitializeDeviceParameter()
        {
            devInterface = new Guid("b0c54dd5-59d9-41c1-82c9-4568ae8ff1de");
            //         iRevNo = 1;
            //       strSerialNumber = "314a385b3532";//"Ttl";
            // MyUsbFinder = new UsbDeviceFinder(0x3633, 0x6249, 200, strSerialNumber, devInterface);
            MyUsbFinder = new UsbDeviceFinder(devInterface);
            reader = null;
            writer = null;
            WriterTimeOut = 2;// 50 to 10;
            ReaderTimeOut = 1;//5 to 1
            kbps_20 = Convert.ToByte(18);
            kbps_50 = Convert.ToByte(19);
            kbps_100 = Convert.ToByte(20);
            kbps_250 = Convert.ToByte(21);
            kbps_300 = Convert.ToByte(22);
            kbps_400 = Convert.ToByte(23);
            kbps_500 = Convert.ToByte(24);
            kbps_600 = Convert.ToByte(25);
            kbps_1000 = Convert.ToByte(26);
            OperatingMode_loopback = Convert.ToByte(36);
            OperatingMode_normal = Convert.ToByte(35);
            OperatingMode_listen_only = Convert.ToByte(37);
            RxMsgOnly = Convert.ToByte(1);
            TxMsgOnly = Convert.ToByte(2);
            MultipleMsg = Convert.ToByte(0);
           
            

            //    IsDeviceConnected = false;
            //    vCANhardwareConnected = false;//abhishek
            // viewfrm = new FormView();
        }

        public void InitializeReader()
        {
            if (MyUsbDevice != null)
            {
                // open read endpoint 1.
                reader = MyUsbDevice.OpenEndpointReader(ReadEndpointID.Ep01, 20, EndpointType.Bulk);
            }
            else
            {
                // it will come here if read end point failed to open.

            }
        }
        public void InitializeWriter()
        {
            if (MyUsbDevice != null)
            {
                // open write endpoint 1.
                writer = MyUsbDevice.OpenEndpointWriter(WriteEndpointID.Ep01, EndpointType.Bulk);
            }
            else
            {
                // it will come here if write end point failed to open.

            }
        }

        public bool vCAN_ConnectDevice()
        {
            /// ErrorCode ec = ErrorCode.None;
            bool bConnected = false;
            try
            {
                // Find and open the usb device.
                MyUsbDevice = UsbDevice.OpenUsbDevice(MyUsbFinder);
                // If the device is open and ready
                if (MyUsbDevice == null)
                {
                    bConnected = false;
                }
                else
                {
                    //Console.WriteLine("Device Found and Open Success");
                    InitializeReader();
                    InitializeWriter();
                    bConnected = true;
                }
            }
            catch (Exception ex)
            {
                bConnected = false;
            }
            return bConnected;
        } //End of public bool vCAN_connectDevice

        public void vCAN_DisconnectDevice()
        {
            if (MyUsbDevice != null)
            {
                if (MyUsbDevice.IsOpen)
                {
                    MyUsbDevice.Close();
                }

            }
            UsbDevice.Exit();
        }// End of public bool vCAN_DisconnectDvice

        public string vCAN_getFirmWareVersion()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            byte[] RecieveVersioninfo = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = 129;

            SendDataOnUsb(transmitBuffer1);

            Thread.Sleep(20);
            RecieveVersioninfo = ReadDataFromUsb();
            if (RecieveVersioninfo[0] == 171) //171 is the valid reply from the device
            {
                string tmp1 = RecieveVersioninfo[1].ToString("X2");
                string tmp2 = RecieveVersioninfo[2].ToString("X2");
                string sHWversion = tmp1.Substring(0, 1) + "." + tmp1.Substring(1, 1);
                string sFirmwareversion = tmp2.Substring(0, 1) + "." + tmp2.Substring(1, 1);
                //convert Byte to Hexa and display as decimal : 0X01 to .1
                // lbl_HWVersion.Text = "H/W:" + sHWversion;
                // lbl_Firmware.Text = "F/W:" + sFirmwareversion;
                return sFirmwareversion;
            }
            else
            {
                /*lbl_HWVersion.Text = "H/W:Error";
                lbl_Firmware.Text = "F/W:Error";
                MessageBox.Show("WARNING: Please Replug the Device");*/
                return null;
            }
        }// End of vCAN_getFirmWareVersion.

        public string vCAN_getHardWareVersion()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            byte[] RecieveVersioninfo = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = 129;

            SendDataOnUsb(transmitBuffer1);

            Thread.Sleep(20);
            RecieveVersioninfo = ReadDataFromUsb();
            if (RecieveVersioninfo[0] == 171) //171 is the valid reply from the device
            {
                string tmp1 = RecieveVersioninfo[1].ToString("X2");
                string tmp2 = RecieveVersioninfo[2].ToString("X2");
                string sHWversion = tmp1.Substring(0, 1) + "." + tmp1.Substring(1, 1);
                string sFirmwareversion = tmp2.Substring(0, 1) + "." + tmp2.Substring(1, 1);

                return sHWversion;
            }
            else
            {

                return null;
            }
        }// End of vCAN_getHardWareVersion.

        private void SendDataOnUsb(byte[] data)
        {
            ErrorCode ec = ErrorCode.None;
            int bytesWritten = 0;

            try
            {
                if (data != null)
                    ec = writer.Write(data, WriterTimeOut, out bytesWritten);
                //else
                //ec = writer.Write(data, WriterTimeOut, out bytesWritten);     // abhishek        
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                // Console.WriteLine("SendDataForWrite called");
            }
        }// End of SendDataOnUsb.

        private byte[] ReadDataFromUsb()
        {
            ErrorCode ec = ErrorCode.None;
            byte[] readBuffer = new byte[20];
            int bytesRead;
            try
            {
                ec = reader.Read(readBuffer, ReaderTimeOut, out bytesRead);//abhishek
                if (bytesRead != 0)
                {
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine((ec != ErrorCode.None ? ec + ":" : String.Empty) + ex.Message);
                // MessageBox.Show("Exception:" + (ec != ErrorCode.None ? ec + ":" : String.Empty) + ex.Message);
            }
            finally
            {
                Console.WriteLine("ReadData called");
            }

            return readBuffer;
        }// End of ReadDataFromUsb

        public void vcan_SetBaudrate_250kbps()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(17);
            transmitBuffer1[1] = kbps_250;
            SendDataOnUsb(transmitBuffer1);

        }// End of 250 fixed baudrate
        public void vcan_SetBaudrate_500kbps()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(17);
            transmitBuffer1[1] = kbps_500;
            SendDataOnUsb(transmitBuffer1);

        }// End of 500 fixed baudrate
        public void vcan_SetBaudrate_1000kbps()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(17);
            transmitBuffer1[1] = kbps_1000;
            SendDataOnUsb(transmitBuffer1);

        }// End of 1000 fixed baudrate

        public void vCAN_Set_operatingMode_normal()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(34);
            transmitBuffer1[1] = OperatingMode_normal;
            SendDataOnUsb(transmitBuffer1);
        }// ENd of vCANSet_operatingMode_normal.

        public void vCAN_Set_operatingMode_listen_only()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(34);
            transmitBuffer1[1] = OperatingMode_listen_only;
            SendDataOnUsb(transmitBuffer1);
        }//ENd of vCANSet_operatingMode_listen_only.
        public void vCAN_Set_operatingMode_loopback()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(34);
            transmitBuffer1[1] = OperatingMode_loopback;
            SendDataOnUsb(transmitBuffer1);
        }//ENd of vCANSet_operatingMode_loopback.

        public void vCAN_Transmit_Msg_11bit_data_frame(byte messageNumber, uint MessageId, byte DLC, byte[] ActualData)
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(49);// comm code for storing the msg is 49.
            transmitBuffer1[1] = messageNumber;
            transmitBuffer1[2] = Convert.ToByte(17);//  11bit can msg
            transmitBuffer1[3] = Convert.ToByte(MessageId >> 24);
            transmitBuffer1[4] = Convert.ToByte(MessageId >> 16);
            transmitBuffer1[5] = Convert.ToByte(MessageId >> 8);
            transmitBuffer1[6] = Convert.ToByte(MessageId & 0xff);
            transmitBuffer1[7] = Convert.ToByte(18); // data frame
            transmitBuffer1[8] = DLC;
            transmitBuffer1[9] = ActualData[0];
            transmitBuffer1[10] = ActualData[1];
            transmitBuffer1[11] = ActualData[2];
            transmitBuffer1[12] = ActualData[3];
            transmitBuffer1[13] = ActualData[4];
            transmitBuffer1[14] = ActualData[5];
            transmitBuffer1[15] = ActualData[6];
            transmitBuffer1[16] = ActualData[7];
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);

            transmitBuffer1[0] = Convert.ToByte(65);// comm code for braudcasting the msg is 65.
            transmitBuffer1[1] = Convert.ToByte(1);
            transmitBuffer1[2] = Convert.ToByte(1);
            SendDataOnUsb(transmitBuffer1);
           

        }// End of vCAN_Transmit_testMsg_11bit_data_frame.
        public void vCAN_Transmit_Msg_29bit_data_frame(byte messageNumber, uint MessageId, byte DLC, byte[] ActualData)
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(49);// comm code for storing the msg is 49.
            transmitBuffer1[1] = messageNumber;
            transmitBuffer1[2] = Convert.ToByte(41);//  29bit can msg
            transmitBuffer1[3] = Convert.ToByte((MessageId >> 24) & 0xff);
            transmitBuffer1[4] = Convert.ToByte((MessageId >> 16) & 0xff);
            transmitBuffer1[5] = Convert.ToByte((MessageId >> 8) & 0xff);
            transmitBuffer1[6] = Convert.ToByte(MessageId & 0xff);
            transmitBuffer1[7] = Convert.ToByte(18); // data frame
            transmitBuffer1[8] = DLC;
            transmitBuffer1[9] = ActualData[0];
            transmitBuffer1[10] = ActualData[1];
            transmitBuffer1[11] = ActualData[2];
            transmitBuffer1[12] = ActualData[3];
            transmitBuffer1[13] = ActualData[4];
            transmitBuffer1[14] = ActualData[5];
            transmitBuffer1[15] = ActualData[6];
            transmitBuffer1[16] = ActualData[7];
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);

            transmitBuffer1[0] = Convert.ToByte(65);// comm code for braudcasting the msg is 65.
            transmitBuffer1[1] = Convert.ToByte(1);
            transmitBuffer1[2] = Convert.ToByte(1);
            SendDataOnUsb(transmitBuffer1);
    

        }// End of vCAN_Transmit_testMsg_29bit_data_frame.
        public void vCAN_Transmit_Msg_11bit_remote_frame(byte messageNumber, uint MessageId, byte DLC, byte[] ActualData)
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(49);// comm code for storing the msg is 49.
            transmitBuffer1[1] = messageNumber;
            transmitBuffer1[2] = Convert.ToByte(41);//  29bit can msg
            transmitBuffer1[3] = Convert.ToByte(MessageId >> 24);
            transmitBuffer1[4] = Convert.ToByte(MessageId >> 16);
            transmitBuffer1[5] = Convert.ToByte(MessageId >> 8);
            transmitBuffer1[6] = Convert.ToByte(MessageId & 0xff);
            transmitBuffer1[7] = Convert.ToByte(19); // remote frame
            transmitBuffer1[8] = DLC;
            transmitBuffer1[9] = ActualData[0];
            transmitBuffer1[10] = ActualData[1];
            transmitBuffer1[11] = ActualData[2];
            transmitBuffer1[12] = ActualData[3];
            transmitBuffer1[13] = ActualData[4];
            transmitBuffer1[14] = ActualData[5];
            transmitBuffer1[15] = ActualData[6];
            transmitBuffer1[16] = ActualData[7];
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);

            transmitBuffer1[0] = Convert.ToByte(65);// comm code for braudcasting the msg is 65.
            transmitBuffer1[1] = Convert.ToByte(1);
            transmitBuffer1[2] = Convert.ToByte(1);
            SendDataOnUsb(transmitBuffer1);
     

        }// End of vCAN_Transmit_testMsg_11bit_remote_frame.
        public void vCAN_Transmit_Msg_29bit_remote_frame(byte messageNumber, uint MessageId, byte DLC, byte[] ActualData)
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(49);// comm code for storing the msg is 49.
            transmitBuffer1[1] = messageNumber;
            transmitBuffer1[2] = Convert.ToByte(41);//  29bit can msg
            transmitBuffer1[3] = Convert.ToByte((MessageId >> 24) & 0xff);
            transmitBuffer1[4] = Convert.ToByte((MessageId >> 16) & 0xff);
            transmitBuffer1[5] = Convert.ToByte((MessageId >> 8) & 0xff);
            transmitBuffer1[6] = Convert.ToByte(MessageId & 0xff);
            transmitBuffer1[7] = Convert.ToByte(19); // remote frame
            transmitBuffer1[8] = DLC;
            transmitBuffer1[9] = ActualData[0];
            transmitBuffer1[10] = ActualData[1];
            transmitBuffer1[11] = ActualData[2];
            transmitBuffer1[12] = ActualData[3];
            transmitBuffer1[13] = ActualData[4];
            transmitBuffer1[14] = ActualData[5];
            transmitBuffer1[15] = ActualData[6];
            transmitBuffer1[16] = ActualData[7];
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);

            transmitBuffer1[0] = Convert.ToByte(65);// comm code for braudcasting the msg is 65.
            transmitBuffer1[1] = Convert.ToByte(1);
            transmitBuffer1[2] = Convert.ToByte(1);
            SendDataOnUsb(transmitBuffer1);
     
        }// End of vCAN_Transmit_testMsg_29bit_remote_frame.

        public byte[] vCAN_Receive_Msg()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            byte[] ReceivedMsg = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = 0x61;// comm code for receiving  the msg is 97 (0x61).
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);
            ReceivedMsg = ReadDataFromUsb();
            return ReceivedMsg;
        }// End of vCAN_Receive_Msg.

        public void vCANSetFilterNone()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(38);// comm code for storing the msg is 38.
            transmitBuffer1[1] = Convert.ToByte(0);
            transmitBuffer1[2] = Convert.ToByte(0);
            transmitBuffer1[3] = Convert.ToByte(0);
            transmitBuffer1[4] = Convert.ToByte(0);
            transmitBuffer1[5] = Convert.ToByte(0);
            transmitBuffer1[6] = Convert.ToByte(0);
            transmitBuffer1[7] = Convert.ToByte(0);
            transmitBuffer1[8] = Convert.ToByte(0);
            transmitBuffer1[9] = Convert.ToByte(0);
            transmitBuffer1[10] = Convert.ToByte(0);
            transmitBuffer1[11] = Convert.ToByte(0);
            transmitBuffer1[12] = Convert.ToByte(0);
            transmitBuffer1[13] = Convert.ToByte(0);
            transmitBuffer1[14] = Convert.ToByte(0);
            transmitBuffer1[15] = Convert.ToByte(0);
            transmitBuffer1[16] = Convert.ToByte(0);
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);

            transmitBuffer1[0] = Convert.ToByte(65);// comm code for braudcasting the msg is 65.
            transmitBuffer1[1] = Convert.ToByte(1);
            transmitBuffer1[2] = Convert.ToByte(5);
            SendDataOnUsb(transmitBuffer1);
     
        } // End of vCANSetFilterNone

        public void vCANSetFilterAll_11bit()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(38);// comm code for storing the msg is 38.
            transmitBuffer1[1] = Convert.ToByte(1);
            transmitBuffer1[2] = Convert.ToByte(0);
            transmitBuffer1[3] = Convert.ToByte(0);
            transmitBuffer1[4] = Convert.ToByte(0);
            transmitBuffer1[5] = Convert.ToByte(0);
            transmitBuffer1[6] = Convert.ToByte(0);
            transmitBuffer1[7] = Convert.ToByte(0);
            transmitBuffer1[8] = Convert.ToByte(0);
            transmitBuffer1[9] = Convert.ToByte(0);
            transmitBuffer1[10] = Convert.ToByte(0);
            transmitBuffer1[11] = Convert.ToByte(0);
            transmitBuffer1[12] = Convert.ToByte(0);
            transmitBuffer1[13] = Convert.ToByte(0);
            transmitBuffer1[14] = Convert.ToByte(0);
            transmitBuffer1[15] = Convert.ToByte(0);
            transmitBuffer1[16] = Convert.ToByte(0);
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);

            transmitBuffer1[0] = Convert.ToByte(65);// comm code for braudcasting the msg is 65.
            transmitBuffer1[1] = Convert.ToByte(1);
            transmitBuffer1[2] = Convert.ToByte(5);
            SendDataOnUsb(transmitBuffer1);
   
        }// End of vCANSetFilterAll_11bit


        public void vCANSetFilterAll_29bit()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(38);// comm code for storing the msg is 38.
            transmitBuffer1[1] = Convert.ToByte(2);
            transmitBuffer1[2] = Convert.ToByte(1);
            transmitBuffer1[3] = Convert.ToByte(0);
            transmitBuffer1[4] = Convert.ToByte(0);
            transmitBuffer1[5] = Convert.ToByte(0);
            transmitBuffer1[6] = Convert.ToByte(0);
            transmitBuffer1[7] = Convert.ToByte(0);
            transmitBuffer1[8] = Convert.ToByte(0);
            transmitBuffer1[9] = Convert.ToByte(0);
            transmitBuffer1[10] = Convert.ToByte(0);
            transmitBuffer1[11] = Convert.ToByte(0);
            transmitBuffer1[12] = Convert.ToByte(0);
            transmitBuffer1[13] = Convert.ToByte(0);
            transmitBuffer1[14] = Convert.ToByte(0);
            transmitBuffer1[15] = Convert.ToByte(0);
            transmitBuffer1[16] = Convert.ToByte(0);
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);

            transmitBuffer1[0] = Convert.ToByte(65);// comm code for braudcasting the msg is 65.
            transmitBuffer1[1] = Convert.ToByte(1);
            transmitBuffer1[2] = Convert.ToByte(5);
            SendDataOnUsb(transmitBuffer1);
      
        } //End  of vCANSetFilter_29bit

        public void SetCustomFilter_11bit(uint MaskId, uint Filter1, uint Filter2, uint Filter3)
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(38);// comm code for storing the msg is 38.
            transmitBuffer1[1] = Convert.ToByte(1);
            transmitBuffer1[2] = Convert.ToByte(0);
            transmitBuffer1[3] = Convert.ToByte(0);
            transmitBuffer1[4] = Convert.ToByte(0);
            transmitBuffer1[5] = Convert.ToByte(0);
            transmitBuffer1[6] = Convert.ToByte(0);
            transmitBuffer1[7] = Convert.ToByte(0);
            transmitBuffer1[8] = Convert.ToByte(0);
            transmitBuffer1[9] = Convert.ToByte(0);
            transmitBuffer1[10] = Convert.ToByte(0);
            transmitBuffer1[11] = Convert.ToByte(0);
            transmitBuffer1[12] = Convert.ToByte(0);
            transmitBuffer1[13] = Convert.ToByte(0);
            transmitBuffer1[14] = Convert.ToByte(0);
            transmitBuffer1[15] = Convert.ToByte(0);
            transmitBuffer1[16] = Convert.ToByte(0);
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);

            transmitBuffer1[0] = Convert.ToByte(65);// comm code for braudcasting the msg is 65.
            transmitBuffer1[1] = Convert.ToByte(1);
            transmitBuffer1[2] = Convert.ToByte(5);
            SendDataOnUsb(transmitBuffer1);
     
        } // End of SetCustomFilter_11bit

        public void SetCustomFilter_29bit(uint MaskId, uint Filter1, uint Filter2, uint Filter3)
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(38);// comm code for storing the msg is 38.
            transmitBuffer1[1] = Convert.ToByte(2);
            transmitBuffer1[2] = Convert.ToByte(1);
            transmitBuffer1[3] = Convert.ToByte(0);
            transmitBuffer1[4] = Convert.ToByte(0);
            transmitBuffer1[5] = Convert.ToByte(0);
            transmitBuffer1[6] = Convert.ToByte(0);
            transmitBuffer1[7] = Convert.ToByte(0);
            transmitBuffer1[8] = Convert.ToByte(0);
            transmitBuffer1[9] = Convert.ToByte(0);
            transmitBuffer1[10] = Convert.ToByte(0);
            transmitBuffer1[11] = Convert.ToByte(0);
            transmitBuffer1[12] = Convert.ToByte(0);
            transmitBuffer1[13] = Convert.ToByte(0);
            transmitBuffer1[14] = Convert.ToByte(0);
            transmitBuffer1[15] = Convert.ToByte(0);
            transmitBuffer1[16] = Convert.ToByte(0);
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);

            transmitBuffer1[0] = Convert.ToByte(65);// comm code for braudcasting the msg is 65.
            transmitBuffer1[1] = Convert.ToByte(1);
            transmitBuffer1[2] = Convert.ToByte(5);
            SendDataOnUsb(transmitBuffer1);
       
        } // End of SetCustomFilter_29bit


        public void vCANClearDevicBuffer()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = 0xcb;// comm code for clear device buffer (0xcb).
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);
        } // End of clearbufferdevice

        public void vCANSetBufferBehaviourBoth()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = 0xbb;// comm code for vCANSetBufferBehaviourBoth (0xbb).
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);
        } // End of vCANSetBufferBehaviourBoth

        public void vCANSetRxMsgOnly()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = 0xbb;// comm code for vCANSetBufferBehaviourBoth (0xbb).
            transmitBuffer1[1] = RxMsgOnly;// receive msg only (1)
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);
        } // End of vCANSetRxMsgOnly


        public void vCANSetTxMsgOnly()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = 0xbb;// comm code for vCANSetBufferBehaviourBoth (0xbb).
            transmitBuffer1[1] = TxMsgOnly;
            transmitBuffer1[2] = Convert.ToByte(0);;// Transmit msg only (2)
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);
        } // End of vCANSetTxMsgOnly

        public void vCANMultipleMsg()
        {
            byte[] transmitBuffer1 = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = 0x62;// comm code for vCANSetBufferBehaviourBoth (0x662).
            transmitBuffer1[1] = Convert.ToByte(0);
            transmitBuffer1[2] = Convert.ToByte(0);
            transmitBuffer1[3] = MultipleMsg;// multiple msg (3).
            SendDataOnUsb(transmitBuffer1);
            Thread.Sleep(1);
        } // End of vCANMultipleMsg


        public void vCANDLLVersion()
        {
            DllVer = "1.2";
            
        } // End of vCANDLLVersion


    }

}
